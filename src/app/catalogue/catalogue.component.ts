import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss']
})
export class CatalogueComponent implements OnInit {
  productsCart: any;
  ngOnInit()  {
   
  }
  products:any;
  
  constructor( ){
    this.productsCart=[];
    this.products=[
      {id:1001, name:"Onions",   price:83.00, weight:'1kg', imgsrc:"assets/Images/1001.jpg" ,count:'1'},
      {id:1002, name:"Sugar ", price:29.00,weight:'1kg', imgsrc:"assets/Images/1002.jpg",count:'1'},
     {id:1003, name:"mango",  price:39.00, weight:'1kg', imgsrc:"assets/Images/1003.jpeg",count:'1'},
  {id:1004, name:"banana",  price:49.00, weight:'12pcs', imgsrc:"assets/Images/1004.jpeg",count:'1'},
    {id:1005, name:"Bitterguard",    price:48.00,weight:'1kg', imgsrc:"assets/Images/1005.jpeg",count:'1'},
    {id:1006, name:"Potato",    price:60.00,weight:'1kg', imgsrc:"assets/Images/1006.jpg",count:'1'}
   ];
    
  }
  addToCart(prod: any) {
    console.log(prod);

    this.productsCart.push(prod);
    localStorage.setItem("cartProds", JSON.stringify(this.productsCart));

  }

}
