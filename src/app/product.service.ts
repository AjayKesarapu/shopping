import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  cartItems=[];
  constructor(private http: HttpClient) { 
    this.cartItems=[];
  }
  addToCart(product:never) {
    this.cartItems.push(product);
  }
}
