import { Component } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent {
  cartItems: any;
  temp: any;
  total: number;
  // count: number;
  // count=0;
  count =0;
  products:any;
  
  constructor() {
    this.total = 0;
    //this.count=1;

    this.temp = localStorage.getItem("cartProds");
    this.cartItems = JSON.parse(this.temp);

    this.cartItems.forEach((products: any) => {
      this.total = this.total + products.price;
    });

  }

  ngOnInit() {
  }

  deleteFromCart(prod: any) {
    const i = this.cartItems.findIndex((product: any) => {
      return product.id == prod.id;
    });

    this.total = this.total - this.cartItems[i].price;

    this.cartItems.splice(i, 1);
    localStorage.setItem("cartProds", JSON.stringify(this.cartItems));
  }

  add(prod:any){
    const i = this.cartItems.findIndex((product: any) => {
      return product.id == prod.id;
    });
    this.cartItems[i].count++;
    this.total = this.total + this.cartItems[i].price;
  }
   
  subtract(prod:any){
    const i = this.cartItems.findIndex((product: any) => {
      return product.id == prod.id;
    });
    this.cartItems[i].count--;
    this.total = this.total - this.cartItems[i].price;
  }
  checkOut() {
    this.total = 0;
    this.cartItems = null;
    localStorage.setItem("cartProds", JSON.stringify(this.cartItems));
  }
 
}

